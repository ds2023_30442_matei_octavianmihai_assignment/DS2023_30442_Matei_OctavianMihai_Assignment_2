import * as React from 'react'

import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import { LineChart } from '@mui/x-charts/LineChart'

function getDate (current_date) {
  const month = current_date.getMonth() + 1
  const year = current_date.getFullYear()
  const day = current_date.getDate()
  return `${day}/${month}/${year}`
}

function getDateHourly (current_date) {
  const hours = current_date.getHours() + 1
  const minutes = current_date.getMinutes()
  const seconds = current_date.getSeconds()
  return `${hours}:${minutes}:${seconds}`
}

function computeData (data, relationship_id, current_date) {
  var relationship_data = data.find(item => item.pairId === relationship_id)
  if (!relationship_data.deviceEntityArrayList) return {}
  var todays_data = {}
  for (let i = 0; i < relationship_data.deviceEntityArrayList.length; i++) {
    var d = new Date(0)
    d.setUTCSeconds(relationship_data.deviceEntityArrayList[i].timestamp)
    if (getDate(d) === getDate(current_date)) {
      todays_data[getDateHourly(d)] =
        relationship_data.deviceEntityArrayList[i].usage
    }
  }
  return todays_data
}

function drawData (data, relationship_id, current_date, max_consumption) {
  if (data && relationship_id && current_date) {
    let sth = computeData(data, relationship_id, current_date)
    if (Object.keys(sth) && Object.keys(sth).length === 0)
      return 'No data for this date'
    return (
      <LineChart
        xAxis={[
          {
            id: 'ids',
            data: Object.keys(sth),
            scaleType: 'band',
            label: 'Time [HH:mm:ss]'
          }
        ]}
        series={[
          {
            label: 'Energy value [kWh]',
            data: Object.values(sth),
            showMark: Object.values(sth).length > 10 ? false : true
          },
          {
            label: 'Maximum Consumption [kWh]',
            data: Array(Object.values(sth).length).fill(max_consumption),
            showMark: false,
            color: 'red'
          }
        ]}
        width={500}
        height={300}
      />
    )
  }
}

export default function Chart (props) {
  let dialog = (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby='alert-dialog-title'
      aria-describedby='alert-dialog-description'
      PaperProps={{
        sx: {
          minWidth: '30%',
          minheight: 300,
          width: 'auto',
          maxHeight: 'auto'
        }
      }}
    >
      <DialogTitle id='alert-dialog-title'>
        {props.title} for {getDate(props.target_date)}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id='alert-dialog-description'>
          {drawData(
            props.data,
            props.target_relationship,
            props.target_date,
            props.max_consumption
          )}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.handleClose} autoFocus>
          {' '}
          Close{' '}
        </Button>
      </DialogActions>
    </Dialog>
  )

  if (!props.open) dialog = null

  return dialog
}
