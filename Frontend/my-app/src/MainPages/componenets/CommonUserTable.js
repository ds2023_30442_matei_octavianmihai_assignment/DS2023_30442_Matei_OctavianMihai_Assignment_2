import React, { useState, useEffect } from 'react'
import { over } from 'stompjs'
import SockJS from 'sockjs-client'

import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  Typography
} from '@mui/material'
import config from '../../config.json'
import LocalStorageHelper from '../../common/localStorageMethods'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

import Chart from '../../pop_messages/Chart'

var stompClient = null
function CommonUserTable () {
  const [data, setData] = useState([])
  const [messages, addMessages] = useState([])
  const [selected_relationship, setRelationship] = useState(null)
  const [selected_max_usage, setMaxUsage] = useState(null)
  const [userData, setUserData] = useState({
    from: LocalStorageHelper.getJwtUser(),
    connected: false
  })
  const [open, setOpen] = React.useState(false)

  const connect = () => {
    let Sock = new SockJS(config.consumerApi)
    stompClient = over(Sock)
    stompClient.connect({}, onConnected, onError)
  }
  const onConnected = () => {
    setUserData({ ...userData, connected: true })
    stompClient.subscribe(
      '/user/' + userData.from + '/private',
      onPrivateMessage
    )
    sendPrivateValue()
  }
  const sendPrivateValue = () => {
    if (stompClient) {
      console.log('Sent private message!')
      stompClient.send('/app/private-message', {}, JSON.stringify(userData))
    }
  }
  const onError = err => {
    if (stompClient) {
      stompClient.disconnect()
      setUserData({ ...userData, connected: false })
    }
  }
  const onPrivateMessage = payload => {
    if (stompClient) {
      var payloadData = JSON.parse(payload.body)
      let obj = JSON.parse(payloadData.text)
      addMessages(obj)
    }
  }

  const getIfOver = relationshipId => {
    var messages_get = messages.find(item => item.pairId === relationshipId)
    if (messages_get)
      return messages_get.alertConsumption
        ? 'rgba(255, 0, 0, 0.5)'
        : 'rgba(0, 100, 52, 0.5)'
    else return 'rgba(0, 100, 52, 0.5)'
  }

  const calculateTotalConsumption = relationshipId => {
    var messages_get = messages.find(item => item.pairId === relationshipId)
    if (messages_get && messages_get.deviceEntityArrayList) {
      return messages_get.deviceEntityArrayList[
        messages_get.deviceEntityArrayList.length - 1
      ].usage
    }
    return 0
  }

  useEffect(() => {
    if (!userData.connected) {
      connect()
    } else {
      sendPrivateValue()
    }
  }, [messages])

  useEffect(() => {
    // Fetch data from your API when the component mounts
    const requestOptions = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    }
    fetch(
      config.productApi +
        'userproduct/get_by_id/' +
        LocalStorageHelper.getJwtUser() +
        '/' +
        LocalStorageHelper.getUser().id,
      requestOptions
    )
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)
        setData(response.data)
      })
      .catch()
  }, [data, messages])

  const [edit_check_date, set_edit_check_date] = useState(new Date())

  return (
    <div>
      <Chart
        id='user-chart'
        open={open}
        title='Consumption'
        data={messages}
        max_consumption={selected_max_usage}
        target_date={edit_check_date}
        target_relationship={selected_relationship}
        handleClose={() => {
          setOpen(false)
        }}
      ></Chart>
      <div>
        <Typography variant='h3' gutterBottom>
          List of Products
        </Typography>
      </div>
      <TableContainer
        component={Paper}
        sx={{ maxHeight: 440, maxWidth: '90%', margin: 'auto' }}
      >
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
            <TableRow>
              <TableCell>Relationship ID</TableCell>
              <TableCell>Product Name</TableCell>
              <TableCell>Max Consumption</TableCell>
              <TableCell>Over Maximum</TableCell>
              <TableCell>See graph</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(row => (
              <TableRow key={row.userProductDto.id}>
                <TableCell>{row.userProductDto.id}</TableCell>
                <TableCell>{row.productDto.productName}</TableCell>
                <TableCell>{row.productDto.maxUsage}</TableCell>
                <TableCell
                  style={{ backgroundColor: getIfOver(row.userProductDto.id) }}
                >
                  {calculateTotalConsumption(row.userProductDto.id)}
                </TableCell>
                <TableCell>
                  <DatePicker
                    dateFormat='dd/MM/yyyy'
                    className='form-control'
                    selected={edit_check_date}
                    onChange={date => {
                      set_edit_check_date(date)
                    }}
                  />
                  <Button
                    onClick={event => {
                      setRelationship(row.userProductDto.id)
                      setMaxUsage(row.productDto.maxUsage)
                      setOpen(true)
                    }}
                  >
                    See graph
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default CommonUserTable
