# DS2023 30442 Matei Octavian Mihai Assignment 2

## Description of the project

This project is aimed at creating a distributed system with:

- Frontend Project
- User interaction Project
- Product interation Project
- Device consumption Project
- One database per each backend project
- Standalone device simulation Project

In order to do implement these, we will use the following technologies:

- `React` for frontend
- `Spring` and `Java` for backends
- `PostgresSQL` for the database
- `Docker` and `Docker Desktop` for the deployment
- `Python` for device simulator

The goal is to create a website that allows the users to be split into two, an administrator and multiple clients. Each user has different ways of interacting with our system as follows:

### `Client`

The client can register, login and only see its own devices with their consumption. For each device, the user can check charts per each day of consumption.

### `Administrator`

The administrator has more actions such as:

- `CRUD` on all users
- `CRUD` on all devices
- The ability to `pair` users to devices

<div style="page-break-after: always;"></div>

## Conceptual architecture of the distributed system

![Alt text](Images/ConceptualDiagram.png)
_Fig 1: Conceptual Diagram_

As it can be seen in the image `Fig 1`, The architecture of the project is made up of 4 main elements:

### `The Device Simulator`

The device simulator is a standalone `python` script that takes data from a `.csv` and has a config file that manages:

- the id of the pairing between a user and a device
- the time difference between each "reading"

The script creates an object and sends it on the `device` queue of the RabbitMQ instance.

### `The Frontend`

- The frontend is composed of 2 main components.
  - A `CommonUserTable` that acts as the main page for normal users. A user can see their devices, their respective consumptions, a date picker and a button to display the chart.
  - A `Chart` that displays for the selected date the information in a line chart variation.

The communication between frontend and backend is done using Websockets with the following properties:

- The connection is done on the url: `http://localhost:8083/ws`
- The data subscription is done on: `/user/{jwt key}/private`
- The data sending is done on: `/app/private-message`

### `The Backends`

The backends are Spring applications. They are composed of:

1. `Consumer Backend` that has controllers and services related to `CRUD` operations on the consumer part of the project. This read from `RabbitMQ` two queues for User-Device pairings and respectively device consumption.
2. `Product Backend` that has controllers and services related to `CRUD` operation on the product part of the project

The interation between the two backends is done using a `RabbitMQ` queue. `Product Backend` will send updates concerning the device pairings and device initial data to `RabbitMQ` and `Consumer Backend` will read from `RabbitMQ` updating where it is necessary.

### `The DataBases`

The databases are created in PostgresSQL and have the following structure:

- `Consumer DB` which stores the data for the consumption and has the following tables:
  - User Product that contains pairs of ids Product id - User id and their maximum consumptions
  - Consumption that contains device data from the simulation
- `Product DB` which stores the data for the products and has the following tables:
  - Product Table that contains product information such as name and description
  - User Table that contains only the user's ids fetched from the `User DB`
  - User Product Table that contains pairs of ids Product id - User id

<div style="page-break-after: always;"></div>

## UML Deployment diagram

![Alt text](Images/DeploymentDiagram.png)
_Fig 2: Deployment Diagram_

As it can be seen in `Fig 2`, the deployment is made in Docker having each component of the project in a separate `container`. This allows for greater customizability for each. The main difference in the implementation is that during deployment and actual run of the project, the urls are changed from `localhost` to the one that the component interacts with and the ports are changed according to the one that is mapped in the file. For example:

- For the product backend to access their database, we change:

  - from `localhost:5432\user_db` to `userpostgres\product_db` which is on port `1001`
  - from `localhost:5432\product_db` to `productpostgres\product_db` which is on port `1002`
  - from `localhost:5432\consumer_db` to `consumerpostgres\consumer_db` which is on port `1003`

- For the communication between backends, we needed to do the following change:

  - from `http://localhost:8082/api/usertable/..` to `http://backendproduct:8082/api/usertable/..`

- For the communication between backends and RabbitMQ, we needed to do the following change:

  - from `http://localhost:5672/..` to `http://rabbitmq:5672/..`

  As it can be seen, the `device simulation` project is independent of docker. This allows for a more realistic behaviour, where the simulation/device is independent of the Docker and just sends to the queue information on `localhost`.

## Build and execution considerations

Open cmd in the root folder and run

        docker-compose up --build

After the run, a set of containers should be deployed. In order to see, I recommend using Docker Desktop. Consumer Backend might be blocked by the absence of RabbitMQ, so a restart of the container might be necessary.

In order to access the page, go to:

        http://localhost:3000/

or:

        http://localhost:3000/login

The data for the databases is stored in `root/init_db/...` which is ran at initialization of the database containers

In order to access the already registered users, use their email as password. For example:

        email = admin@admin.com, password = admin@admin.com
        email = dada@gmail.com, password = dada@gmail.com

## Conclusions

In order to create scalable applications that can employ a varied number of people and different technologies, the usage of Docker and container style deployment is necessary. The current project is rather simple in its scope and use, but in reality there can be a variety of services, each written in a different language and uses different types of server, which would be imposible to implement with a monolitic architecture in mind.
