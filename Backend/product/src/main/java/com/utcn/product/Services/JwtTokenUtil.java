package com.utcn.product.Services;

import io.jsonwebtoken.*;
import org.springframework.core.env.Environment;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import java.util.Date;

@Component
public class JwtTokenUtil {

    private String secret_key;

    private final JwtParser jwtParser;

    public JwtTokenUtil(Environment environment){
        secret_key = environment.getProperty("jwt.secret");
        this.jwtParser = Jwts.parser().setSigningKey(secret_key);
    }

    public boolean isAdmin(String jwt){
        if(!isLogged(jwt)) return false;

        Claims data = this.parseJwtClaims(jwt);
        if(validateClaims(data)) return data.getSubject().equals("admin@admin.com");
        else return false;
    }

    public boolean isLogged(String jwt){
        Claims data = this.parseJwtClaims(jwt);
        return validateClaims(data);
    }

    public Claims parseJwtClaims(String token) {
        return jwtParser.parseClaimsJws(token).getBody();
    }

    public boolean validateClaims(Claims claims) throws AuthenticationException {
        try {
            return claims.getExpiration().after(new Date());
        } catch (Exception e) {
            throw e;
        }
    }
}
