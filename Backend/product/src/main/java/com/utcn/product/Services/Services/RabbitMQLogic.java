package com.utcn.product.Services.Services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.utcn.product.Common.Mapper.MapStructMapperImpl;
import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Data.dto.UserProductDto;
import com.utcn.product.Data.entity.Product;
import com.utcn.product.Data.entity.RabbitMqEntity;
import com.utcn.product.Data.entity.UserProduct;
import com.utcn.product.Data.repository.ProductRepository;
import com.utcn.product.Data.repository.UserProductRepository;
import lombok.Getter;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

@Component
public class RabbitMQLogic {

    private final ConnectionFactory factory;

    @Getter
    private final String queueName;

    private final UserProductRepository userProductRepository;
    private final ProductRepository productRepository;

    public RabbitMQLogic(RabbitMqEntity rabbitMqEntity, Environment environment, UserProductRepository userProductRepository, ProductRepository productRepository) {
        this.userProductRepository = userProductRepository;
        this.productRepository = productRepository;

        factory = new ConnectionFactory();
        factory.setHost(rabbitMqEntity.getHost());
        factory.setPort(rabbitMqEntity.getPort());
        factory.setUsername(rabbitMqEntity.getUsername());
        factory.setPassword(rabbitMqEntity.getPassword());
        queueName = environment.getProperty("rabbitmq.queue-name");
        initialStartup();
    }

    public void initialStartup(){
        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();

        ArrayList<UserProduct> userProducts = (ArrayList<UserProduct>) userProductRepository.findAll();
        ArrayList<UserProductDto> userTableServiceAll = mapStructMapper.userProductListTouserProductDtoList(userProducts);

        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(new UserProductDto());
            this.writeToRabbitMq(this.queueName, 0, RabbitStatus.PURGE, json);
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }

        userTableServiceAll.forEach(userProductDto -> {
            try {
                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String json = ow.writeValueAsString(userProductDto);
                Optional<Product> productDtoOptional = productRepository.findById(userProductDto.getProductId());
                int max_usage = productDtoOptional.orElseGet(Product::new).getMaxUsage();
                this.writeToRabbitMq(this.queueName, max_usage, RabbitStatus.NEW, json);
            } catch (IOException | TimeoutException e) {
                throw new RuntimeException(e);
            }
        });
        System.out.println("Finished User Product pairs");

    }

    public void writeToRabbitMq(String queue_name, int max_usage, RabbitStatus rabbitStatus, String message) throws IOException, TimeoutException{
        message = "{\"max_usage\" : "+max_usage+", \n\"status\" : \""+ rabbitStatus.toString() + "\",\n\"object_payload\" :" + message +"}";

        try{
            int maxAttempts = 50;
            Connection connection = this.makeConnection(maxAttempts);
            Channel channel = connection.createChannel();
            channel.queueDeclare(queue_name, false, false, false, null);
            channel.basicPublish("", queue_name, null, message.getBytes());
            System.out.println(" [x] Sent:\n" + message);
            channel.close();
            connection.close();
        }catch(InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private Connection makeConnection(int maxAttempts) throws TimeoutException, InterruptedException {
        int attempts = 0;
        Connection connection = null;

        while (connection == null && attempts < maxAttempts) {
            try {
                connection = factory.newConnection();
                // If no exception is thrown, the connection is successful
            } catch (Exception e) {
                attempts++;
                System.out.println("Connection attempt #" + attempts + " failed. Retrying in 5 seconds...");
                Thread.sleep(5000);
                // You can add a delay here using Thread.sleep if needed
            }
        }
        if (connection != null) {
            System.out.println("Connection successful!");
            return connection;
            // Continue with your logic using the 'connection' object
        } else {
            throw new TimeoutException();
        }
    }

}


