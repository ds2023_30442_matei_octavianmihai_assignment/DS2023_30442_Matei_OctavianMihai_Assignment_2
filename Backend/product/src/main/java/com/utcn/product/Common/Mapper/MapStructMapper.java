package com.utcn.product.Common.Mapper;

import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Data.dto.UserProductDto;
import com.utcn.product.Data.dto.UserTableDto;
import com.utcn.product.Data.entity.Product;
import com.utcn.product.Data.entity.UserProduct;
import com.utcn.product.Data.entity.UserTable;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.ArrayList;

@Mapper(
        componentModel = "spring"
)
public interface MapStructMapper {

    Product productDtoToProduct(ProductDto product);
    ProductDto productToProductDto(Product product);

    ArrayList<ProductDto> productArrayToProductDtoArray(ArrayList<Product> products);

    UserProduct userProductDtoToUserProduct(UserProductDto userProductDto);

    UserProductDto userProductToUserProductDto(UserProduct userProduct);

    ArrayList<UserProductDto> userProductListTouserProductDtoList(ArrayList<UserProduct> userProducts);
}
