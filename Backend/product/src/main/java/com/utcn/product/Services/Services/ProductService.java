package com.utcn.product.Services.Services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.utcn.product.Common.Exceptions.InvalidDataException;
import com.utcn.product.Common.Mapper.MapStructMapperImpl;
import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Data.entity.Product;
import com.utcn.product.Data.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    private final RabbitMQLogic rabbitMQLogic;

    public ProductService(ProductRepository productRepository, RabbitMQLogic rabbitMQLogic) {
        this.productRepository = productRepository;
        this.rabbitMQLogic = rabbitMQLogic;
    }

    public ArrayList<ProductDto> getAll(){
        ArrayList<Product> productArrayList = productRepository.findAll();
        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();

        return mapStructMapper.productArrayToProductDtoArray(productArrayList);

    }

    public void updateUser(ProductDto productDto) throws Exception{
        Product productClass;
        if(productDto.getId() != null){
            Optional<Product> optionalProduct = productRepository.findById(productDto.getId());
            productClass = optionalProduct.orElseGet(Product::new);
        }else {
            productClass = new Product();
        }
        productClass.setProductName(productDto.getProductName());
        productClass.setDescription(productDto.getDescription());
        productClass.setMaxUsage(productDto.getMaxUsage());
        productRepository.saveAndFlush(productClass);

        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        ProductDto productDto1 = mapStructMapper.productToProductDto(productClass);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(productDto1);

        rabbitMQLogic.writeToRabbitMq(rabbitMQLogic.getQueueName(), productDto1.getMaxUsage(), RabbitStatus.UPDATE, json);
    }

    public void deleteProduct(Long id) throws InvalidDataException {
        Optional<Product> optionalProduct = productRepository.findById(id);

        if(optionalProduct.isPresent()){
            try {
                productRepository.deleteById(id);

                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String json = ow.writeValueAsString(optionalProduct.get());

                rabbitMQLogic.writeToRabbitMq(rabbitMQLogic.getQueueName(), 0 , RabbitStatus.DELETE, json);
            }catch (Exception ex){
                throw new InvalidDataException("Cannot delete now");
            }
        }else{
            throw new InvalidDataException("No product found");
        }
    }

}
