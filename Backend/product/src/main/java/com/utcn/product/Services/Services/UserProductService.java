package com.utcn.product.Services.Services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.utcn.product.Common.Mapper.MapStructMapperImpl;
import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Data.dto.RelationshipDto;
import com.utcn.product.Data.dto.UserProductDto;
import com.utcn.product.Data.entity.Product;
import com.utcn.product.Data.entity.UserProduct;
import com.utcn.product.Data.repository.ProductRepository;
import com.utcn.product.Data.repository.UserProductRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

@Service
public class UserProductService {
    private final UserProductRepository userProductRepository;
    private final ProductRepository productRepository;
    private final RabbitMQLogic rabbitMQLogic;

    public UserProductService(UserProductRepository userProductRepository, ProductRepository productRepository, RabbitMQLogic rabbitMQLogic) {
        this.userProductRepository = userProductRepository;
        this.productRepository = productRepository;
        this.rabbitMQLogic = rabbitMQLogic;
    }

    public ArrayList<UserProductDto> getAll(){
        ArrayList<UserProduct> userProducts = (ArrayList<UserProduct>) userProductRepository.findAll();

        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        return mapStructMapper.userProductListTouserProductDtoList(userProducts);
    }

    public ArrayList<RelationshipDto> getAllByUserId(Long id){
        ArrayList<RelationshipDto> relationshipDtoArrayList = new ArrayList<>();
        ArrayList<UserProduct> userTableArray = userProductRepository.findByUserId(id);
        for(UserProduct userProduct : userTableArray){
            Optional<Product> product = productRepository.findById(userProduct.getProductId());
            if(product.isPresent()){
                MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
                ProductDto productDto = mapStructMapper.productToProductDto(product.get());
                UserProductDto userProductDto = mapStructMapper.userProductToUserProductDto(userProduct);
                RelationshipDto relationshipDto = new RelationshipDto(userProductDto, productDto);
                relationshipDtoArrayList.add(relationshipDto);
            }
        }
        return relationshipDtoArrayList;
    }

    public void updateRelationship(UserProductDto userProductDto) throws IOException, TimeoutException {
        Optional<UserProduct> userProductOptional = userProductRepository.findByRelationship(userProductDto.getId());
        UserProduct userProduct;
        if(userProductOptional.isPresent()){
            userProductOptional.get().setUserId(userProductDto.getUserId());
            userProductOptional.get().setProductId(userProductDto.getProductId());
            userProduct = userProductOptional.get();
        }else{
            MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
            userProduct = mapStructMapper.userProductDtoToUserProduct(userProductDto);
        }
        userProductRepository.saveAndFlush(userProduct);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(userProduct);
        Optional<Product> product = productRepository.findById(userProduct.getProductId());
        rabbitMQLogic.writeToRabbitMq(rabbitMQLogic.getQueueName(), product.get().getMaxUsage(), RabbitStatus.UPDATE, json);
    }

    public void deleteRelationship(Long id) {
        Optional<UserProduct> userProductOptional = userProductRepository.findById(id);
        userProductOptional.ifPresent(userProduct -> {
            userProductRepository.delete(userProduct);
            try {

                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String json = ow.writeValueAsString(userProduct);

                rabbitMQLogic.writeToRabbitMq(rabbitMQLogic.getQueueName(), 0, RabbitStatus.DELETE, json);
            } catch (IOException | TimeoutException e) {
                e.printStackTrace();
            }
        });
    }
}
