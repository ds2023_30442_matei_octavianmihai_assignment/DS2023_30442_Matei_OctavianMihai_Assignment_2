package com.utcn.product.Data.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RelationshipDto {
    private UserProductDto userProductDto;
    private ProductDto productDto;
}
