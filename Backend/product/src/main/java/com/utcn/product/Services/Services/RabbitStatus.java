package com.utcn.product.Services.Services;

public enum RabbitStatus {
    NEW,
    UPDATE,
    DELETE,
    PURGE
}
