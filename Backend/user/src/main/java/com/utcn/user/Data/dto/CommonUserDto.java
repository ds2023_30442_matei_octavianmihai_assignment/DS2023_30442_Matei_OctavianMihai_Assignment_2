package com.utcn.user.Data.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Getter
@Setter
@Service
public class CommonUserDto implements Serializable {
    private Long id;
    private String email;
    private String password;
    private String description;


    @Override
    public String toString() {
        return "CommonUserDto{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
