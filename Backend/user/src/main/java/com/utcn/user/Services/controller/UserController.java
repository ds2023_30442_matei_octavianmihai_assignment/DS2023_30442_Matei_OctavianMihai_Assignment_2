package com.utcn.user.Services.controller;

import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.utcn.user.Common.Exceptions.InvalidDataException;
import com.utcn.user.Data.dto.CommonUserDto;
import com.utcn.user.Data.dto.WebsiteDTO;
import com.utcn.user.Services.Response.ApiResponse;
import com.utcn.user.Services.Response.ApiResponseBuilder;
import com.utcn.user.Services.Security.JwtTokenUtil;
import com.utcn.user.Services.Services.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final JwtTokenUtil jwtTokenUtil;


    public UserController(UserService userService, JwtTokenUtil jwtTokenUtil) {
        this.userService = userService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @GetMapping("/get_all_users/{jwt}")
    public ResponseEntity<ApiResponse> getAllUsers(@PathVariable("jwt") String jwt) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");

        try {
            if(jwtTokenUtil.isAdmin(jwt)){
                ArrayList<CommonUserDto> allUsers = userService.getAllUsers();
                return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                        .withHttpHeader(httpHeaders)
                        .withData(allUsers)
                        .build();
            }
            else{
                throw new InvalidDataException("User is not authorised to make this request!");
            }
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @PatchMapping("/update_user")
    public ResponseEntity<ApiResponse> updateUser(@RequestBody ObjectNode objectNode) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            String jwt = objectNode.get("jwt").asText();
            ObjectMapper jsonObjectMapper = new ObjectMapper();
            CommonUserDto commonUserDto = jsonObjectMapper.treeToValue(objectNode.get("commonUserDto"), CommonUserDto.class);

            if(jwtTokenUtil.isAdmin(jwt)){
                WebsiteDTO updateUser = userService.updateUser(commonUserDto);

                if(updateUser != null){
                    return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                            .withHttpHeader(httpHeaders)
                            .withData(true)
                            .build();
                }else{
                    throw new InvalidDataException("Cannot update the user!");
                }
            }
            else{
                throw new InvalidDataException("User is not authorised to make this request!");
            }
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @DeleteMapping("/delete_user/{jwt}/{id}")
    public ResponseEntity<ApiResponse> deleteUser(@PathVariable("jwt") String jwt, @PathVariable("id") Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            if(jwtTokenUtil.isAdmin(jwt)){
                userService.deleteUser(jwt, id);
                return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                        .withHttpHeader(httpHeaders)
                        .withData(true)
                        .build();
            }
            else{
                throw new InvalidDataException("User is not authorised to make this request!");
            }
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }
}
