package com.utcn.consumer.Data.rabbitmq;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeviceData  implements Serializable {

    @JsonProperty("timestamp")
    private Double timestamp;
    @JsonProperty("pair_id")
    private Long pairId;
    @JsonProperty("measured_value")
    private Double usage;

    @Override
    public String toString() {
        return "DeviceData{" +
                "timestamp=" + timestamp +
                ", pairId=" + pairId +
                ", value=" + usage +
                '}';
    }
}
