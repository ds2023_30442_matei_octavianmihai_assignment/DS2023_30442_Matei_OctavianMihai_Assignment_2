package com.utcn.consumer.Common.Exceptions;

public class InvalidDataException extends Exception {
    public InvalidDataException(String message) {
        super(message);
    }
}
