package com.utcn.consumer.Data.repository;

import com.utcn.consumer.Data.entity.UserProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface UserProductEntityRepository extends JpaRepository<UserProductEntity, Long> {

    @Transactional
    void deleteAllByProductId(int productId);

    List<UserProductEntity> findAllByProductId(int productId);

    List<UserProductEntity> findAllByUserId(int userId);

}