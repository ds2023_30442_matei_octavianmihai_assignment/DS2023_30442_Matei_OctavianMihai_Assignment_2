package com.utcn.consumer.Data.rabbitmq;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqEntity {

    @Value("${spring.rabbitmq.host}")
    private String host;

    @Value("${spring.rabbitmq.port}")
    private int port;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Bean
    public String getHost() {
        return host;
    }

    @Bean
    public int getPort() {
        return port;
    }

    @Bean
    public String getUsername() {
        return username;
    }

    @Bean
    public String getPassword() {
        return password;
    }
}
