package com.utcn.consumer.Data.dto;

import com.utcn.consumer.Data.entity.DeviceEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Setter
@Getter
public class ConsumptionDto {

    private int pairId;
    private int maxUsage;
    private boolean alertConsumption;
    private ArrayList<DeviceEntity> deviceEntityArrayList;

    public void addDevice(DeviceEntity device){
        if(deviceEntityArrayList == null){
            deviceEntityArrayList = new ArrayList<>();
        }
        deviceEntityArrayList.add(device);
    }
}
