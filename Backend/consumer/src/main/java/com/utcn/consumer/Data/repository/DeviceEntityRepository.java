package com.utcn.consumer.Data.repository;

import com.utcn.consumer.Data.entity.DeviceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceEntityRepository extends JpaRepository<DeviceEntity, Long> {

    List<DeviceEntity> findAllByPairId(int pairId);
}