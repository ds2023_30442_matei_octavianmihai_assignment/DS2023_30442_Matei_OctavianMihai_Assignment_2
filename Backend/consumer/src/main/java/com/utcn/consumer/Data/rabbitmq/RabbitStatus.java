package com.utcn.consumer.Data.rabbitmq;

public enum RabbitStatus {
    NEW,
    UPDATE,
    DELETE,
    PURGE
}
