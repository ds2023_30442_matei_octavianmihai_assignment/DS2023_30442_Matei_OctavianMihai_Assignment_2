package com.utcn.consumer.Services.Services.RabbitMQLogic;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utcn.consumer.Data.rabbitmq.DeviceData;
import com.utcn.consumer.Data.rabbitmq.RabbitMqEntity;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;


@Component
public class RabbityMQConsumerDevices  extends RabbitMQConsumer{

    private final DeviceService deviceService;
    private final String property_queue = "rabbitmq.queue-name-device";

    public RabbityMQConsumerDevices(Environment environment, RabbitMqEntity rabbitMqEntity, DeviceService deviceService) {
        super(rabbitMqEntity);
        this.deviceService = deviceService;
        this.initConsumer(environment.getProperty(property_queue));
    }

    @SneakyThrows
    @RabbitListener(queues = "${"+property_queue+"}")
    public void recievedMessage(String json) {
        ObjectMapper mapper = new ObjectMapper();
        DeviceData deviceData = mapper.readValue(json, DeviceData.class);
        System.out.println("Received Message From RabbitMQ: " + deviceData);
        deviceService.decideDevice(deviceData);
    }

}
