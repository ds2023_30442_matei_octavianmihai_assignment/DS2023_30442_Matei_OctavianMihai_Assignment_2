package com.utcn.consumer.Common.Mapper;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

public class MyDoubleDesirializer extends JsonSerializer<Double> {


    @Override
    public void serialize(Double value, JsonGenerator gen, SerializerProvider serializers)
            throws IOException, JsonProcessingException {
        // TODO Auto-generated method stub

        BigDecimal d = new BigDecimal(value);
        gen.writeNumber(d.toPlainString());
    }

}
