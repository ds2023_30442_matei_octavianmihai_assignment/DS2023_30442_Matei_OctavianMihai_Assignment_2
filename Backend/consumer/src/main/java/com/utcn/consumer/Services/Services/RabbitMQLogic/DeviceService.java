package com.utcn.consumer.Services.Services.RabbitMQLogic;

import com.utcn.consumer.Common.Mapper.MapStructMapperImpl;
import com.utcn.consumer.Data.entity.DeviceEntity;
import com.utcn.consumer.Data.rabbitmq.DeviceData;
import com.utcn.consumer.Data.repository.DeviceEntityRepository;
import org.springframework.stereotype.Service;

@Service
public class DeviceService {

    private final DeviceEntityRepository deviceEntityRepository;

    public DeviceService(DeviceEntityRepository deviceEntityRepository) {
        this.deviceEntityRepository = deviceEntityRepository;
    }

    public void decideDevice(DeviceData deviceData){
        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        DeviceEntity device = mapStructMapper.deviceDataToDeviceEntity(deviceData);
        deviceEntityRepository.saveAndFlush(device);
    }
}
