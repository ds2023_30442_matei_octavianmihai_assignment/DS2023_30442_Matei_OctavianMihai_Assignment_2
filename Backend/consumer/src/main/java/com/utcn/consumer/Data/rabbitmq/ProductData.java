package com.utcn.consumer.Data.rabbitmq;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductData implements Serializable {

    @JsonProperty("status")
    private String rabbitStatus;
    @JsonProperty("max_usage")
    private int max_usage;
    @JsonProperty("object_payload")
    private UserProductDto objectPayload;

    @Override
    public String toString() {
        return "ProductData{" +
                ", rabbitStatus='" + rabbitStatus + '\'' +
                ", max_usage='" + max_usage + '\'' +
                ", objectPayload=" + objectPayload +
                '}';
    }
}
