package com.utcn.consumer.Data.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Entity
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_product", schema="consumer_db")
public class UserProductEntity {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "user_id", nullable = false)
    private int userId;
    @Column(name = "product_id", nullable = false)
    private int productId;
    @Column(name = "max_usage", nullable = false)
    private int maxUsage;

    @Override
    public String toString() {
        return "UserProductEntity{" +
                "id=" + id +
                ", userId=" + userId +
                ", productId=" + productId +
                ", maxUsage=" + maxUsage +
                '}';
    }
}
