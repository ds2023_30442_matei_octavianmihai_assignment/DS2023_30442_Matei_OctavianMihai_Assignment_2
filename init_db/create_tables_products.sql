CREATE SCHEMA IF NOT EXISTS product_db;

-- Set the search path to include the 'product_db' schema
SET search_path TO product_db;

----------------------------------------------------------------------------------- product table creation
-- Drop the table if it exists
DROP TABLE IF EXISTS product_db.products;

-- Create a new table in the product_db schema
CREATE TABLE product_db.products
(
    ID           SERIAL       PRIMARY KEY,
    PRODUCT_NAME text NOT NULL,
    DESCRIPTION  text,
	MAX_USAGE integer
);

-- Set the owner of the table
ALTER TABLE product_db.products OWNER TO "compose-postgres";
----------------------------------------------------------------------------------- user table clone creation
-- Drop the table if it exists
DROP TABLE IF EXISTS product_db.user_table;

-- Create a new table in the product_db schema
CREATE TABLE product_db.user_table
(
    ID           SERIAL       PRIMARY KEY
);

-- Set the owner of the table
ALTER TABLE product_db.user_table OWNER TO "compose-postgres";

----------------------------------------------------------------------------------- user product relationship creation
-- Drop the table if it exists
DROP TABLE IF EXISTS product_db.user_product;

-- Create a new table in the product_db schema
CREATE TABLE product_db.user_product
(
    ID           SERIAL       PRIMARY KEY,
    USER_ID    integer      NOT NULL
		constraint user_product_user_id_fk
				references product_db.user_table
				on update cascade on delete cascade,
    PRODUCT_ID integer      NOT NULL
		constraint user_product_products_id_fk
				references product_db.products
				on update cascade on delete cascade
);

-- Set the owner of the table
ALTER TABLE product_db.user_product OWNER TO "compose-postgres";

----------------------------------------------------------------------------------- Data insertion
insert into product_db.products (product_name, description, max_usage) values ('smartphone', 'A phone that is smart', 100);
insert into product_db.products (product_name, description, max_usage) values ('washing machine', null, 200);
insert into product_db.products (product_name, description, max_usage) values ('toaster', null, 100);
insert into product_db.products (product_name, description, max_usage) values ('microwave machine', null, 100);
insert into product_db.products (product_name, description, max_usage) values ('blinds', null, 200);


insert into product_db.user_table (id) values (1);
insert into product_db.user_table (id) values (2);
insert into product_db.user_table (id) values (3);
insert into product_db.user_table (id) values (4);
insert into product_db.user_table (id) values (5);

insert into product_db.user_product (user_id, product_id) values (5, 1);
insert into product_db.user_product (user_id, product_id) values (5, 5);
insert into product_db.user_product (user_id, product_id) values (5, 5);
insert into product_db.user_product (user_id, product_id) values (2, 5);
insert into product_db.user_product (user_id, product_id) values (2, 1);


-- Reset the search path to its default value
RESET search_path;
